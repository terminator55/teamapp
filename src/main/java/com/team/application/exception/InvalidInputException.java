package com.team.application.exception;

public class InvalidInputException extends Exception {
	public InvalidInputException(String exception) {
		super(exception);
	}

	public InvalidInputException() {
		super();
	}
}
