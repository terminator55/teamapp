package com.team.application.service;

import java.util.List;

import com.team.application.dto.CreateTeamDTO;
import com.team.application.dto.TeamUpdateDTO;
import com.team.application.exception.BlankFieldException;
import com.team.application.exception.InvalidInputException;
import com.team.application.exception.TeamDoesNotExistException;
import com.team.application.exception.UserDoesNotExistException;
import com.team.application.model.Team;

public interface TeamService {

	public void createTeam(CreateTeamDTO createTeamDTO) throws BlankFieldException, UserDoesNotExistException, InvalidInputException;

	public Team updateTeam(String teamId, TeamUpdateDTO teamUpdateDTO) throws BlankFieldException, TeamDoesNotExistException, InvalidInputException;

	public void deleteTeam(String teamId) throws TeamDoesNotExistException;
	
	public List<Team> listAllTeams();
	
	public Team viewTeam(String teamId) throws TeamDoesNotExistException;
	
	public List<Team> listTeamsByUser(String userId) throws UserDoesNotExistException;
	
	public List<String> listTeamMembers(String teamId) throws TeamDoesNotExistException;
	
	public void removeMembersFromTeam(String teamId, List<String> userIdList) throws TeamDoesNotExistException;

	public void addMembersToTeam(String teamId, List<String> userIdList) throws TeamDoesNotExistException;
	
	public List<Team> listStarredTeams(List<String> teamIdList);
	
}
