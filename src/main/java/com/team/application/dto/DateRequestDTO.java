package com.team.application.dto;

import java.util.Date;

import org.springframework.format.annotation.DateTimeFormat;

public class DateRequestDTO {

	private String taskId;
	@DateTimeFormat
	private Date date;

	/**
	 * @return the taskId
	 */
	public String getTaskId() {
		return taskId;
	}

	/**
	 * @param taskId the taskId to set
	 */
	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}
	
}
