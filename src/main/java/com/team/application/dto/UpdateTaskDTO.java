package com.team.application.dto;

import java.util.Date;

import org.springframework.format.annotation.DateTimeFormat;

public class UpdateTaskDTO {

	private String taskId;
	private String taskName;
	private String taskDescription;
	@DateTimeFormat
	private Date taskUpdatedOn;

	/**
	 * @return the taskId
	 */
	public String getTaskId() {
		return taskId;
	}
	/**
	 * @param taskId the taskId to set
	 */
	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}
	/**
	 * @return the taskName
	 */
	public String getTaskName() {
		return taskName;
	}
	/**
	 * @param taskName the taskName to set
	 */
	public void setTaskName(String taskName) {
		this.taskName = taskName;
	}
	/**
	 * @return the taskDescription
	 */
	public String getTaskDescription() {
		return taskDescription;
	}
	/**
	 * @param taskDescription the taskDescription to set
	 */
	public void setTaskDescription(String taskDescription) {
		this.taskDescription = taskDescription;
	}
	public Date getTaskUpdatedOn() {
		return taskUpdatedOn;
	}
	public void setTaskUpdatedOn(Date taskUpdatedOn) {
		this.taskUpdatedOn = taskUpdatedOn;
	}


}
