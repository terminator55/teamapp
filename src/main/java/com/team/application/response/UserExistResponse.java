package com.team.application.response;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;

public class UserExistResponse implements Serializable {

	private static final long serialVersionUID = 1L;

	@SerializedName("status")
	private boolean status;

	@SerializedName("message")
	private String message;

	@SerializedName("isMobile")
	private boolean isMobile;

	@SerializedName("userId")
	private String userId;

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public boolean isStatus() {
		return status;
	}

	public void setStatus(boolean status) {
		this.status = status;
	}

	@JsonProperty("isMobile")
	public boolean isMobile() {
		return isMobile;
	}

	public void setMobile(boolean isMobile) {
		this.isMobile = isMobile;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public UserExistResponse(boolean status, String message, boolean isMobile, String userId) {
		super();
		this.status = status;
		this.message = message;
		this.isMobile = isMobile;
		this.userId = userId;
	}

}
