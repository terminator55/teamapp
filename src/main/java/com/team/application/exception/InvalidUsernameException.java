package com.team.application.exception;

public class InvalidUsernameException extends Exception {

	public InvalidUsernameException(String exception) {
		super(exception);
	}

	public InvalidUsernameException() {
		super();
	}
}
