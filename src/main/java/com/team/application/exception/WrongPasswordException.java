package com.team.application.exception;

public class WrongPasswordException extends Exception {
	public WrongPasswordException(String exception) {
		super(exception);
	}

	public WrongPasswordException () {
		super();
		
	}
}
