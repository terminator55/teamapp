package com.team.application.dto;

import java.util.List;

public class StarredTaskRequestDTO {

	private List<String> taskIdList;

	public List<String> getTaskIdList() {
		return taskIdList;
	}

	public void setTaskIdList(List<String> taskIdList) {
		this.taskIdList = taskIdList;
	}
}
