package com.team.application.dto;

import java.util.List;

public class CreateUserDTO {

	private String userName;
	private String emailId;
	private String imageUrl;
	private String mobileNo;
	private String password;
	private List<String> teamList;
	private List<String> taskList;
	
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public String getEmailId() {
		return emailId;
	}
	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}
	public String getImageUrl() {
		return imageUrl;
	}
	public void setImageUrl(String imageUrl) {
		this.imageUrl = imageUrl;
	}
	public String getMobileNo() {
		return mobileNo;
	}
	public void setMobileNo(String mobileNo) {
		this.mobileNo = mobileNo;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public List<String> getTeamList() {
		return teamList;
	}
	public void setTeamList(List<String> teamList) {
		this.teamList = teamList;
	}
	public List<String> getTaskList() {
		return taskList;
	}
	public void setTaskList(List<String> taskList) {
		this.taskList = taskList;
	}
}