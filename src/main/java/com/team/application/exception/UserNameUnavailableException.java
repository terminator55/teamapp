package com.team.application.exception;

public class UserNameUnavailableException extends Exception {
	public UserNameUnavailableException(String exception) {
		super(exception);
	}

	public UserNameUnavailableException () {
		super();
		
	}
}
