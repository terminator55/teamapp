package com.team.application.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.team.application.constants.AppConstants;
import com.team.application.dto.DateRequestDTO;
import com.team.application.dto.StarredTaskRequestDTO;
import com.team.application.dto.TaskDTO;
import com.team.application.dto.UpdateTaskDTO;
import com.team.application.exception.BlankFieldException;
import com.team.application.exception.InvalidInputException;
import com.team.application.exception.TaskDoesNotExistException;
import com.team.application.exception.TeamDoesNotExistException;
import com.team.application.exception.UserDoesNotExistException;
import com.team.application.response.TeamAppResponse;
import com.team.application.service.TaskService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@RestController
@Component
@Api(tags = "Task-API")
@RequestMapping("/tasks")
public class TaskController {

	@Autowired
	TaskService taskService;

	@ApiOperation(value = "Create Task", notes = "Create a new task")
	@RequestMapping(value = "/create",method = RequestMethod.POST)
	@ResponseStatus(HttpStatus.CREATED)
	public TeamAppResponse createTask(@RequestBody TaskDTO createTaskRequest) throws BlankFieldException, InvalidInputException, TeamDoesNotExistException, UserDoesNotExistException {
		try {
			taskService.createTask(createTaskRequest);
			return new TeamAppResponse("New task Created",AppConstants.Status.SUCCESS, null);
		} catch (BlankFieldException e) {
			return new TeamAppResponse("Resource field(s) null", AppConstants.ERROR, null);
		} catch (InvalidInputException e) {
			return new TeamAppResponse("Invalid input value(s)", AppConstants.ERROR, null);
		} catch (TeamDoesNotExistException e) {
			return new TeamAppResponse("Team Id doesn't exist", AppConstants.ERROR, null);
		} catch (UserDoesNotExistException e) {
			return new TeamAppResponse("User Id does't exist", AppConstants.ERROR, null);
		}
	}
	
	@ApiOperation(value = "Update Task", notes = "Update Task name and description")
	@RequestMapping(value = "update", method = RequestMethod.PUT)
	@ResponseStatus(HttpStatus.OK)
	public TeamAppResponse updateTask(@RequestBody UpdateTaskDTO updateTaskRequest)
			throws BlankFieldException, TaskDoesNotExistException, InvalidInputException {
		try {
			return new TeamAppResponse("Task Updated",AppConstants.Status.SUCCESS, taskService.updateTask(updateTaskRequest));
		} catch (InvalidInputException e) {
			return new TeamAppResponse("Invalid input value(s)", AppConstants.ERROR, null);
		} catch (BlankFieldException e) {
			return new TeamAppResponse("Resource field(s) null", AppConstants.ERROR, null);
		} catch (TaskDoesNotExistException e) {
			return new TeamAppResponse("Task Id doesn't exist", AppConstants.ERROR, null);
		}
	}
	
	@ApiOperation(value = "Change Task Status", notes = "Change Task Status")
	@RequestMapping(value = "changeStatus/{taskId}", method = RequestMethod.PUT)
	@ResponseStatus(HttpStatus.OK)
	public TeamAppResponse changeStatus(@PathVariable String taskId, String status) throws TaskDoesNotExistException, InvalidInputException {
		
			try {
				taskService.changeStatus(taskId,status);
				return new TeamAppResponse("Status changed",AppConstants.Status.SUCCESS, null);
			} catch (TaskDoesNotExistException e) {
				return new TeamAppResponse("Task Id doesn't exist", AppConstants.ERROR, null);
			} catch (InvalidInputException e) {
				return new TeamAppResponse("Invalid input value(s)", AppConstants.ERROR, null);
			}
	}	

	@ApiOperation(value = "Delete Task", notes = "Delete Task")
	@RequestMapping(value = "delete/{taskId}", method = RequestMethod.DELETE)
	@ResponseStatus(HttpStatus.OK)
	public TeamAppResponse deleteTask(@PathVariable String taskId) throws TaskDoesNotExistException {
		try {
			taskService.deleteTask(taskId);
			return new TeamAppResponse("Task deleted",AppConstants.Status.SUCCESS, null);
		} catch (TaskDoesNotExistException e) {
			return new TeamAppResponse("Task Id doesn't exist", AppConstants.ERROR, null);
		}
	}
	
	@ApiOperation(value = "List all Tasks", notes = "List all tasks")
	@RequestMapping(method = RequestMethod.GET)
	@ResponseStatus(HttpStatus.OK)
	public TeamAppResponse listAllTasks() {
		return new TeamAppResponse("Task deleted",AppConstants.Status.SUCCESS, taskService.listAllTasks());
	}
	
	@ApiOperation(value = "List team tasks", notes = "List tasks by teamId")
	@RequestMapping(value = "/listTeamTasks/{teamId}",method = RequestMethod.POST)
	@ResponseStatus(HttpStatus.OK)
	public TeamAppResponse listTeamTasks(@PathVariable String teamId) throws TeamDoesNotExistException  {
		
		try {
			return new TeamAppResponse("Task list",AppConstants.Status.SUCCESS, taskService.listTeamTasks(teamId));
		} catch (TeamDoesNotExistException e) {
			return new TeamAppResponse("Task Id doesn't exist", AppConstants.ERROR, null);
		}
	}
	
	@ApiOperation(value = "List user tasks", notes = "List tasks by userId")
	@RequestMapping(value = "/listUserTasks/{userId}",method = RequestMethod.POST)
	@ResponseStatus(HttpStatus.OK)
	public TeamAppResponse listUserTasks(@PathVariable String userId) throws UserDoesNotExistException {

		try {
			return new TeamAppResponse("Task list",AppConstants.Status.SUCCESS, taskService.listUserTasks(userId));
		} catch (UserDoesNotExistException e) {
			return new TeamAppResponse("User Id does't exist", AppConstants.ERROR, null);
		}
	}
	
	@ApiOperation(value = "List user team tasks", notes = "List tasks by userId and teamId")
	@RequestMapping(value = "/listUserTeamTasks/{teamId}/{userId}",method = RequestMethod.POST)
	@ResponseStatus(HttpStatus.OK)
	public TeamAppResponse listUserTeamTasks(@PathVariable String teamId,@PathVariable String userId) throws TeamDoesNotExistException, UserDoesNotExistException {

		try {
			return new TeamAppResponse("Task list",AppConstants.Status.SUCCESS, taskService.listUserTeamTasks(teamId, userId));
		} catch (TeamDoesNotExistException e) {
			return new TeamAppResponse("Team Id doesn't exist", AppConstants.ERROR, null);
		} catch (UserDoesNotExistException e) {
			return new TeamAppResponse("User Id does't exist", AppConstants.ERROR, null);
		}
	}
	
	@ApiOperation(value = "View Task", notes = "View task by Id")
	@RequestMapping(value = "/{taskId}", method = RequestMethod.POST)
	@ResponseStatus(HttpStatus.OK)
	public TeamAppResponse viewTask(@PathVariable String taskId) throws TaskDoesNotExistException {
		try {
			return new TeamAppResponse("Task list",AppConstants.Status.SUCCESS, taskService.viewTask(taskId));
		} catch (TaskDoesNotExistException e) {
			return new TeamAppResponse("Task Id doesn't exist", AppConstants.ERROR, null);
		}
	}
	
	@ApiOperation(value = "Change due date", notes = "Change due date")
	@RequestMapping(value = "/changeDueDate", method = RequestMethod.PUT)
	@ResponseStatus(HttpStatus.OK)
	public TeamAppResponse changeDueDate(@RequestBody DateRequestDTO dueDateRequest) throws TaskDoesNotExistException, InvalidInputException {
		
			try {
				taskService.changeDueDate(dueDateRequest);
				return new TeamAppResponse("Date changed",AppConstants.Status.SUCCESS, null);
			} catch (TaskDoesNotExistException e) {
				return new TeamAppResponse("Task Id doesn't exist", AppConstants.ERROR, null);
			} catch (InvalidInputException e) {
				return new TeamAppResponse("Invalid input value(s)", AppConstants.ERROR, null);
			}
	}
	
	@ApiOperation(value = "Change assignee", notes = "Change assignee")	
	@RequestMapping(value = "changeAssignee", method = RequestMethod.PUT)
	@ResponseStatus(HttpStatus.OK)
	public TeamAppResponse changeAssignee(@RequestBody String taskId, String assigneeId) throws TaskDoesNotExistException, UserDoesNotExistException {
		
			try {
				taskService.changeAssignee(taskId, assigneeId);
				return new TeamAppResponse("Assignee changed",AppConstants.Status.SUCCESS, null);
			} catch (TaskDoesNotExistException e) {
				return new TeamAppResponse("Task Id doesn't exist", AppConstants.ERROR, null);
			} catch (UserDoesNotExistException e) {
				return new TeamAppResponse("User Id does't exist", AppConstants.ERROR, null);
			}
	}
	
	@ApiOperation(value = "List starred tasks", notes = "List starred tasks")
	@RequestMapping(value = "/starred", method = RequestMethod.POST)
	@ResponseStatus(HttpStatus.OK)
	public TeamAppResponse listStarredTasks(@RequestBody StarredTaskRequestDTO starredTaskRequest){

		return new TeamAppResponse("Assignee changed",AppConstants.Status.SUCCESS, taskService.listStarredTasks(starredTaskRequest.getTaskIdList()));
	}
	
}