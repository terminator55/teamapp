package com.team.application.exception;

public class InvalidMobileNumberException extends Exception {
	public InvalidMobileNumberException(String exception) {
		super(exception);
	}

	public InvalidMobileNumberException() {
		super();
	}
}
