package com.team.application.exception;

public class ImageExceedsSizeException extends Exception {

	  public ImageExceedsSizeException(String exception) {
		    super(exception);
	 }
	  public ImageExceedsSizeException() {
		    super();
	 }
}
