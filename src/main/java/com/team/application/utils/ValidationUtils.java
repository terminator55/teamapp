package com.team.application.utils;

import java.util.regex.Pattern;

import org.apache.commons.lang3.StringEscapeUtils;

public class ValidationUtils {   
	/**
     * Constructor.
     */
    private ValidationUtils() {    }   

    public static boolean isValidName(String name) {
        return Pattern.compile("^[a-zA-Z][a-zA-Z ]+$")
                .matcher(name).find();
    }
    
	public static boolean isValidUsername(String userName) {
        return Pattern.compile("^[a-zA-Z0-9._-]{3,15}$")
                .matcher(userName).find();
	}    
    
    public static boolean isValidEmail(String name) {
        return Pattern.compile("^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$")
                .matcher(name).find();
    }
    
    public static boolean isNullValue(String name) {
        return name.isEmpty();
    } 

    public static String validate(String unsafeString) {
        return StringEscapeUtils.escapeHtml4(unsafeString);   
    }
    
    public static boolean isValidMobileNumber(String id) {
        return  (id.length()==10) && ((Pattern.compile("^[0-9]+$").matcher(id).find())); 
    }
    
    public static void main(String[] args) {
    	System.out.println(isValidName("q  a  "));
/*    	System.out.println(isValidMobileNumber("kj"));
    	System.out.println(isValidEmail("singhgm_ail@qwe.comas"));*/
    }

}
