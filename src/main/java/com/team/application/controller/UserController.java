package com.team.application.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.team.application.constants.AppConstants;
import com.team.application.dto.LoginDTO;
import com.team.application.dto.UpdateProfileDTO;
import com.team.application.dto.UserPasswordDto;
import com.team.application.exception.BlankFieldException;
import com.team.application.exception.ConfirmPasswordDoesNotMatchException;
import com.team.application.exception.FbUserException;
import com.team.application.exception.InvalidEmailException;
import com.team.application.exception.InvalidInputException;
import com.team.application.exception.InvalidMobileNumberException;
import com.team.application.exception.InvalidUsernameException;
import com.team.application.exception.OldPasswordException;
import com.team.application.exception.UserDoesNotExistException;
import com.team.application.exception.WrongPasswordException;
import com.team.application.response.TeamAppResponse;
import com.team.application.service.UserService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@RestController
@Component
@Api(tags = "User-API")
@RequestMapping("/users")
public class UserController {

	@Autowired
	private UserService userService;
	
	private final Logger log = LoggerFactory.getLogger(UserController.class);
	
	@CrossOrigin
	@ApiOperation(value = "Login Service", notes = "Login service: for user login")
	@RequestMapping(value = "/login", method = RequestMethod.POST)
	@ResponseStatus(HttpStatus.OK)
	public TeamAppResponse loginUser(@RequestBody LoginDTO userCredential) {
		log.info("Inside UserController : loginUser");
		try {
			return new TeamAppResponse(AppConstants.ResponseMessages.USER_LOGGED_IN_SUCCESSFULLY,
					AppConstants.Status.SUCCESS, userService.userLogin(userCredential));
		} catch (BlankFieldException e) {
			return new TeamAppResponse(AppConstants.ResponseMessages.BLANK_FIELDS, AppConstants.ERROR, null);
		} catch (UserDoesNotExistException e) {
			return new TeamAppResponse(AppConstants.ResponseMessages.USER_DOES_NOT_EXIST, AppConstants.ERROR, null);
		} catch (FbUserException e) {			
			return new TeamAppResponse(AppConstants.ResponseMessages.FB_USER, AppConstants.ERROR, null);
		} catch (WrongPasswordException e) {
			return new TeamAppResponse(AppConstants.ResponseMessages.WRONG_PASSWORD, AppConstants.ERROR, null);
		} catch (InvalidMobileNumberException e) {
			return new TeamAppResponse(AppConstants.ResponseMessages.INVALID_MOBILE_NUMBER, AppConstants.ERROR, null);
		}
	}
	
	@ApiOperation(value = "Delete user", notes = "It delete the user")
	@RequestMapping(method = RequestMethod.DELETE)
	@ResponseStatus(HttpStatus.OK)
	public TeamAppResponse deleteUser(@RequestParam("userId") String userId) {
		log.info("Inside UserController : deleteUser");
		try {
			userService.deleteUser(userId.trim());
			return new TeamAppResponse("User deleted",AppConstants.Status.SUCCESS, null);
		} catch (UserDoesNotExistException e) {
			return new TeamAppResponse(AppConstants.ResponseMessages.USER_DOES_NOT_EXIST,"BAD_REQUEST", null);
		}
	}
	
/*	@ApiOperation(value = "upload profile image", notes = "It uploads user profile image")
	@RequestMapping(value = "/uploadProfileImage", method = RequestMethod.POST)
	@ResponseStatus(HttpStatus.OK)
	String singleFileUpload(@RequestParam(value = "file", required = true) MultipartFile file,
			@RequestParam("userId") String userId) {

		try {
			return userService.uploadProfileImage(file, userId.trim());
		} catch (UserDoesNotExistException e) {
			e.printStackTrace();
			throw new RuntimeException(AppConstants.ResponseMessages.USER_DOES_NOT_EXIST + ":::" + "BAD_REQUEST");
		} catch (ImageExceedsSizeException e) {
			e.printStackTrace();
			throw new RuntimeException(AppConstants.ResponseMessages.IMAGE_EXCEEDS_SIZE + ":::" + "BAD_REQUEST");
		} catch (IOException e) {
			e.printStackTrace();
			throw new RuntimeException(AppConstants.Status.ERROR + ":::" + "BAD_REQUEST");
		}
	}*/

	
	@ApiOperation(value = "view user profile", notes = "Its used to view the user profile")
	@RequestMapping(value = "/profile", method = RequestMethod.GET)
	@ResponseStatus(HttpStatus.OK)
	public TeamAppResponse viewProfile(@RequestParam("userId") String userId) throws UserDoesNotExistException {

			try {
				return new TeamAppResponse("view user profile",AppConstants.Status.SUCCESS, userService.viewUserProfile(userId));
			} catch (UserDoesNotExistException e) {
				return new TeamAppResponse(AppConstants.ResponseMessages.USER_DOES_NOT_EXIST ,"BAD_REQUEST",null);
			}
	}
	
	@ApiOperation(value = "view other user profile", notes = "Its used to view the other user profile")
	@RequestMapping(value = "/otherProfile", method = RequestMethod.GET)
	@ResponseStatus(HttpStatus.OK)
	public TeamAppResponse viewOtherProfile(@RequestParam("userId") String userId,
			@RequestParam("otherUserId") String otherUserId) {

		try {
			return new TeamAppResponse("view other user profile",AppConstants.Status.SUCCESS, userService.viewOtherUserProfile(userId, otherUserId));
		} catch (UserDoesNotExistException e) {
			return new TeamAppResponse(AppConstants.ResponseMessages.USER_DOES_NOT_EXIST , "BAD_REQUEST",null);
		}
	}
	
	@ApiOperation(value = "List all Users", notes = "List all Users")
	@RequestMapping(value = "/listAll", method = RequestMethod.GET)
	@ResponseStatus(HttpStatus.OK)
	public TeamAppResponse listAll() {
		return new TeamAppResponse("List all Users",AppConstants.Status.SUCCESS, userService.listAllUsers());
	}
	
	@ApiOperation(value = "Change Password", notes = "It changes customer password")
	@RequestMapping(value = "/password", method = RequestMethod.POST)
	@ResponseStatus(HttpStatus.OK)
	public TeamAppResponse changePassword(@RequestBody UserPasswordDto passwordRequest) {
		log.info("Inside UserController : changePassword");
		try {
			userService.changePassword(passwordRequest);
			return new TeamAppResponse("Password changed",AppConstants.Status.SUCCESS, userService.listAllUsers());
		} catch (UserDoesNotExistException e) {
			return new TeamAppResponse(AppConstants.ResponseMessages.USER_DOES_NOT_EXIST ,"BAD_REQUEST",null);
		} catch (FbUserException e) {
			return new TeamAppResponse(AppConstants.ResponseMessages.FB_USER ,"BAD_REQUEST",null);
		} catch (WrongPasswordException e) {
			return new TeamAppResponse(AppConstants.ResponseMessages.WRONG_PASSWORD ,"BAD_REQUEST",null);
		} catch (OldPasswordException e) {
			return new TeamAppResponse(AppConstants.ResponseMessages.OLD_PASSWORD,"BAD_REQUEST",null);
		} catch (ConfirmPasswordDoesNotMatchException e) {
			return new TeamAppResponse(AppConstants.ResponseMessages.CONFIRM_PASSWORD_WRONG ,"BAD_REQUEST",null);
		}
	}
	
	
	@ApiOperation(value = "Edit User Profile", notes = "Edit User Profile")
	@RequestMapping(value = "/editProfile/{userId}", method = RequestMethod.PUT)
	@ResponseStatus(HttpStatus.OK)
	public TeamAppResponse updateProfile(@PathVariable String userId, @RequestBody UpdateProfileDTO updateRequest) {
		try {
			return new TeamAppResponse(AppConstants.ResponseMessages.USERPROFILE_UPDATED_SUCCESSFULLY,
					AppConstants.Status.SUCCESS, userService.editUserProfile(userId, updateRequest));
		} catch (UserDoesNotExistException e) {
			return new TeamAppResponse(AppConstants.ResponseMessages.USER_DOES_NOT_EXIST, AppConstants.ERROR, null);
		} catch (BlankFieldException e) {
			return new TeamAppResponse(AppConstants.ResponseMessages.BLANK_FIELDS, AppConstants.ERROR, null);
		} catch (InvalidInputException e) {
			return new TeamAppResponse(AppConstants.ResponseMessages.INVALID_VALUE, AppConstants.ERROR, null);
		}
	}
	
	@ApiOperation(value = "Edit User Email", notes = "Edit User Email")
	@RequestMapping(value = "/editEmail/{userId}", method = RequestMethod.PUT)
	@ResponseStatus(HttpStatus.OK)
	public TeamAppResponse updateEmail(@PathVariable String userId, @RequestBody String email) {
		try {
			return new TeamAppResponse(AppConstants.ResponseMessages.USERPROFILE_UPDATED_SUCCESSFULLY,
					AppConstants.Status.SUCCESS, userService.editEmail(userId, email));
		} catch (UserDoesNotExistException e) {
			return new TeamAppResponse(AppConstants.ResponseMessages.USER_DOES_NOT_EXIST, AppConstants.ERROR, null);
		} catch (BlankFieldException e) {
			return new TeamAppResponse(AppConstants.ResponseMessages.BLANK_FIELDS, AppConstants.ERROR, null);
		} catch (InvalidEmailException e) {
			return new TeamAppResponse(AppConstants.ResponseMessages.INVALID_EMAIL, AppConstants.ERROR, null);
		}
	}
	
	@ApiOperation(value = "Edit User Mobile", notes = "Edit User Mobile")
	@RequestMapping(value = "/editMobile/{userId}", method = RequestMethod.PUT)
	@ResponseStatus(HttpStatus.OK)
	public TeamAppResponse updateMobile(@PathVariable String userId, @RequestBody String mobile) {
		try {
			return new TeamAppResponse(AppConstants.ResponseMessages.USERPROFILE_UPDATED_SUCCESSFULLY,
					AppConstants.Status.SUCCESS, userService.editMobile(userId, mobile));
		} catch (UserDoesNotExistException e) {
			return new TeamAppResponse(AppConstants.ResponseMessages.USER_DOES_NOT_EXIST, AppConstants.ERROR, null);
		} catch (BlankFieldException e) {
			return new TeamAppResponse(AppConstants.ResponseMessages.BLANK_FIELDS, AppConstants.ERROR, null);
		} catch (InvalidMobileNumberException e) {
			return new TeamAppResponse(AppConstants.ResponseMessages.INVALID_MOBILE_NUMBER, AppConstants.ERROR, null);
		}
	}
	
	@ApiOperation(value = "Edit Username", notes = "Edit Username")
	@RequestMapping(value = "/editUsername/{userId}", method = RequestMethod.PUT)
	@ResponseStatus(HttpStatus.OK)
	public TeamAppResponse updateUsername(@PathVariable String userId, @RequestBody String username) {
		try {
			return new TeamAppResponse(AppConstants.ResponseMessages.USERPROFILE_UPDATED_SUCCESSFULLY,
					AppConstants.Status.SUCCESS, userService.editUsername(userId, username));
		} catch (UserDoesNotExistException e) {
			return new TeamAppResponse(AppConstants.ResponseMessages.USER_DOES_NOT_EXIST, AppConstants.ERROR, null);
		} catch (BlankFieldException e) {
			return new TeamAppResponse(AppConstants.ResponseMessages.BLANK_FIELDS, AppConstants.ERROR, null);
		} catch (InvalidUsernameException e) {
			return new TeamAppResponse(AppConstants.ResponseMessages.INVALID_USERNAME, AppConstants.ERROR, null);
		}
	}
}
