package com.team.application.model;

import java.util.Date;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.format.annotation.DateTimeFormat;

@Document(collection="task")
public class Task {

	@Id
	private String taskId;
	private String teamId;
	private String taskName;
	private String taskDescription;
	
	@DateTimeFormat(pattern = "yyyy-MM-dd'T'HH:mm")
	private Date taskCreatedOn;
	//@DateTimeFormat(pattern = "yyyy-MM-dd'T'HH:mm")
	private Date taskUpdatedOn;
	private String creatorId;
	private String creatorName;
	//private Date taskStartDate;  //createdOn date is sufficient
	@DateTimeFormat(pattern = "yyyy-MM-dd'T'HH:mm")
	private Date taskDueDate;
	private String status;
	private String assigneeId;

	public String getTaskId() {
		return taskId;
	}

	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}

	public String getTeamId() {
		return teamId;
	}

	public void setTeamId(String teamId) {
		this.teamId = teamId;
	}

	public String getTaskName() {
		return taskName;
	}

	public void setTaskName(String taskName) {
		this.taskName = taskName;
	}

	public String getTaskDescription() {
		return taskDescription;
	}

	public void setTaskDescription(String taskDescription) {
		this.taskDescription = taskDescription;
	}

	public Date getTaskCreatedOn() {
		return taskCreatedOn;
	}

	public void setTaskCreatedOn(Date taskCreatedOn) {
		this.taskCreatedOn = taskCreatedOn;
	}



	public Date getTaskUpdatedOn() {
		return taskUpdatedOn;
	}

	public void setTaskUpdatedOn(Date taskUpdatedOn) {
		this.taskUpdatedOn = taskUpdatedOn;
	}

	public Date getTaskDueDate() {
		return taskDueDate;
	}

	public void setTaskDueDate(Date taskDueDate) {
		this.taskDueDate = taskDueDate;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getCreatorId() {
		return creatorId;
	}

	public void setCreatorId(String creatorId) {
		this.creatorId = creatorId;
	}

	public String getCreatorName() {
		return creatorName;
	}

	public void setCreatorName(String creatorName) {
		this.creatorName = creatorName;
	}

	public String getAssigneeId() {
		return assigneeId;
	}

	public void setAssigneeId(String assigneeId) {
		this.assigneeId = assigneeId;
	}
}