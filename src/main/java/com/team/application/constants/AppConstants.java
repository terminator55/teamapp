package com.team.application.constants;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonProperty;

public class AppConstants {

	public static final String ERROR = "ERROR";
	public static final String INFO = "INFO";
	public static final Date DEFAULT_OLD_DATE = new Date(0);
	public static final String SIMPLE_DATE_FORMAT = "dd-MM-yyyy";
	public static final String APP_VERSION_PARTITION_KEY = "avpk";
	public static final String QUESTION_MARK = "?";
	public static final String UNDERSCORE = "_";
	public static final String NEWLINE = "\n";
	public static final boolean TRUE = true;
	public static final boolean FALSE = false;
	

	public static class sortType {
		public static final String ASC = "asc";
		public static final String DESC = "desc";
	}

	public static class ResponseMessages {

		public static final String USER_EXISTS_WITH_THESE_CREDENTIALS = "User exists with these credentials";
		public static final String USER_FIELDS_BLANK = "User fields blank";
		public static final String USER_DOES_NOT_EXIST = "User does not exist";
		public static final String USER_DOES_NOT_EXIST_WITH_MOBILE = "User does not exist with this mobile number";
		public static final String USER_DOES_NOT_EXIST_WITH_EMAIL = "User does not exist with this email id";
		public static final String WRONG_PASSWORD = "Wrong password";
		public static final String OLD_PASSWORD = "Old password";
		public static final String CONFIRM_PASSWORD_WRONG = "Confirm password wrong";
		public static final String INVALID_MOBILE_NUMBER = "Invalid mobile number";
		public static final String USERNAME_ALREADY_TAKEN = "Username already taken!";
		public static final String USER_MOBILE_ALREADY_REGISTERED = "User mobile already registered";
		public static final String USER_EMAIL_ALREADY_REGISTERED = "User email already registered";
		public static final String IMAGE_EXCEEDS_SIZE = "Image exceeds size";
		public static final String SHORT_PASSWORD_LENGTH = "Short password length";
		public static final String INVALID_VALUE = "Invalid value";
		public static final String INVALID_EMAIL = "Invalid email id";
		public static final String INVALID_USERNAME = "Incalid username";
		public static final String USER_LOGGED_IN_SUCCESSFULLY = "User logged in successfully";
		public static final String USER_CREATED = "User created";
		public static final String FB_USER = "facebook user";
		public static final String BLANK_FIELDS = "Blank fields";
		public static final String INVALID_SEARCH_TYPE = "Invalid search type";
		public static final String USERPROFILE_UPDATED_SUCCESSFULLY = "User profile updated successfully";
		public static final String PASSWORD_CHANGED = "Password changed";
		
		
	}

	public static class Status {
		public static final String SUCCESS = "Success";
		public static final int SUCCESS_CODE = 0;
		public static final String ERROR = "Error";
	}

	public static class ProductExperienceConstants {

		public static final String GOOD = "Good";
		public static final String BAD = "Bad";
		public static final String AVERAGE = "Average";
	}

	public enum ProductExperience {
		@JsonProperty("good")
		GOOD {
			@Override
			public String toString() {
				return "good";

			}
		},
		@JsonProperty("bad")
		BAD {
			@Override
			public String toString() {
				return "bad";

			}
		},
		@JsonProperty("average")
		AVERAGE {
			@Override
			public String toString() {
				return "average";

			}
		},

	}

}