package com.team.application.exception;

public class UserMobileExistsException extends Exception {
	public UserMobileExistsException(String exception) {
		super(exception);
	}

	public UserMobileExistsException () {
		super();
		
	}
}
