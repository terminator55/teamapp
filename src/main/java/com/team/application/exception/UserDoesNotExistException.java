package com.team.application.exception;

public class UserDoesNotExistException extends Exception {
	public UserDoesNotExistException(String exception) {
		super(exception);
	}

	public UserDoesNotExistException() {
		super();
	}
}
