package com.team.application.service.impl;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import com.team.application.dto.DateRequestDTO;
import com.team.application.dto.TaskDTO;
import com.team.application.dto.UpdateTaskDTO;
import com.team.application.exception.BlankFieldException;
import com.team.application.exception.InvalidInputException;
import com.team.application.exception.TaskDoesNotExistException;
import com.team.application.exception.TeamDoesNotExistException;
import com.team.application.exception.UserDoesNotExistException;
import com.team.application.model.Task;
import com.team.application.repository.TaskRepository;
import com.team.application.repository.TeamRepository;
import com.team.application.repository.UserRepository;
import com.team.application.service.TaskService;
import com.team.application.utils.ValidationUtils;

@Component
public class TaskServiceImpl implements TaskService {
	
	@Autowired
	TaskRepository taskRepository;
	
	@Autowired
	TeamRepository teamRepository;
	
	@Autowired
	UserRepository userRepository;
	
	private final Logger log = LoggerFactory.getLogger(TaskServiceImpl.class);

	@Override
	public void createTask(TaskDTO createTaskRequest) throws BlankFieldException, InvalidInputException, TeamDoesNotExistException, UserDoesNotExistException {
		if ((!StringUtils.hasText(createTaskRequest.getTeamId()))
				|| (!StringUtils.hasText(createTaskRequest.getTaskName()))
				|| (!StringUtils.hasText(createTaskRequest.getCreatorId()))) {
			throw new BlankFieldException();
		} else {
		Task newTask = new Task();
		if (!teamRepository.exists(createTaskRequest.getTeamId())) {
			throw new TeamDoesNotExistException();
		}
		newTask.setTeamId(createTaskRequest.getTeamId());
		if(!ValidationUtils.isValidName(createTaskRequest.getTaskName())){
			throw new InvalidInputException();
		}
		newTask.setTaskName(createTaskRequest.getTaskName());
		if (!StringUtils.hasText(createTaskRequest.getTaskDescription())){
			newTask.setTaskDescription("");
		}
		newTask.setTaskDescription(createTaskRequest.getTaskDescription());
		newTask.setCreatorId(createTaskRequest.getCreatorId());    //logged in userid
		newTask.setCreatorName(createTaskRequest.getCreatorName());   //loggedin username
		
		newTask.setTaskCreatedOn(createTaskRequest.getTaskCreatedOn());  //local date and time for createdOn
		newTask.setTaskCreatedOn(createTaskRequest.getUpdatedOn());
		newTask.setStatus("open");       //status will be set to open as soon as task is created
		newTask.setTaskDueDate(createTaskRequest.getTaskDueDate());	    ////implement null check in client side, set empty date if no due date is provided
		
		if (!StringUtils.hasText(createTaskRequest.getAssigneeId())){
			newTask.setAssigneeId("");
		}
		else {
			if (!userRepository.exists(createTaskRequest.getAssigneeId())) {
				throw new UserDoesNotExistException();
			}
		}
		newTask.setAssigneeId(createTaskRequest.getAssigneeId());
		
		taskRepository.save(newTask);
		}
	}

	@Override
	public Task updateTask(UpdateTaskDTO updateTaskRequest) throws BlankFieldException, TaskDoesNotExistException, InvalidInputException {
		Task updateTask;
		if ((!StringUtils.hasText(updateTaskRequest.getTaskId()))){
			throw new BlankFieldException();
		}
		else if (!taskRepository.exists(updateTaskRequest.getTaskId())) {
			throw new TaskDoesNotExistException();
		}
		else if (!StringUtils.hasText(updateTaskRequest.getTaskName())){
			throw new BlankFieldException();
		}
		else {
			if (!StringUtils.hasText(updateTaskRequest.getTaskDescription())){
				updateTaskRequest.setTaskDescription("");
			}
			updateTask = taskRepository.findOne(updateTaskRequest.getTaskId());
			if(!ValidationUtils.isValidName(updateTaskRequest.getTaskName())){
				throw new InvalidInputException();
			}
			updateTask.setTaskName(updateTaskRequest.getTaskName());
			updateTask.setTaskDescription(updateTaskRequest.getTaskDescription());
			updateTask.setTaskUpdatedOn(updateTaskRequest.getTaskUpdatedOn());
			taskRepository.save(updateTask);
		}
		return updateTask;

	}

	@Override
	public void changeStatus(String taskId, String status) throws TaskDoesNotExistException, InvalidInputException {
		if (!taskRepository.exists(taskId)) {
			throw new TaskDoesNotExistException();
		} else {
			Task changeStatus = taskRepository.findOne(taskId);
			if(status.equals("closed")||status.equals("open")){
				changeStatus.setStatus(status);
			}
			else{
				throw new InvalidInputException();
			}
			taskRepository.save(changeStatus);
		}
	}

	@Override
	public void deleteTask(String taskId) throws TaskDoesNotExistException {
		if (!taskRepository.exists(taskId)) {
			throw new TaskDoesNotExistException();
		} else {
			taskRepository.delete(taskId);
		}

	}
	
	@Override
	public Task viewTask(String taskId) throws TaskDoesNotExistException{
		if (!taskRepository.exists(taskId)) {
			throw new TaskDoesNotExistException();
		} else {
			return taskRepository.findOne(taskId);
		}
	}
	
	@Override
	public List<Task> listAllTasks() {
		List<Task> list = taskRepository.findAll();
		
		return list;
	}

	@Override
	public List<Task> listTeamTasks(String teamId) throws TeamDoesNotExistException {
		if (!teamRepository.exists(teamId)) {
			throw new TeamDoesNotExistException();
		}
		else{
			return taskRepository.listTeamTasks(teamId);
		}

	}

	@Override
	public List<Task> listUserTasks(String userId) throws UserDoesNotExistException {
		if (!userRepository.exists(userId)) {
			throw new UserDoesNotExistException();
		}
		else{
			return taskRepository.listUserTasks(userId);
		}
	}

	@Override
	public List<Task> listUserTeamTasks(String teamId, String userId) throws UserDoesNotExistException, TeamDoesNotExistException {
		if (!userRepository.exists(userId)) {
			throw new UserDoesNotExistException();
		}
		if (!teamRepository.exists(teamId)) {
			throw new TeamDoesNotExistException();
		}
		else {
			return taskRepository.listUserTeamTasks(teamId, userId);
		}
		
	}

	@Override
	public void changeDueDate(DateRequestDTO dueDateRequest) throws TaskDoesNotExistException, InvalidInputException {
		if (!taskRepository.exists(dueDateRequest.getTaskId())) {
			throw new TaskDoesNotExistException();
		} else {
			Task task = taskRepository.findOne(dueDateRequest.getTaskId());
			if((dueDateRequest.getDate()).compareTo(task.getTaskCreatedOn())<=0){
				throw new InvalidInputException();
			}
			else{
				task.setTaskDueDate(dueDateRequest.getDate());	
			}

			log.info("dueDateRequest.getDate():"+dueDateRequest.getDate());
			taskRepository.save(task);
		}
	}

	@Override
	public void changeAssignee(String taskId, String assigneeId) throws TaskDoesNotExistException, UserDoesNotExistException {
		if (!taskRepository.exists(taskId)) {
			throw new TaskDoesNotExistException();
		}
		if (!userRepository.exists(assigneeId)) {
			throw new UserDoesNotExistException();
		}else {
		Task task = taskRepository.findOne(taskId);
		task.setAssigneeId(assigneeId);
		taskRepository.save(task);
		}
	}
	
	@Override
	public List<Task> listStarredTasks(List<String> taskIdList) {
		List<Task> list = taskRepository.listStarredTasks(taskIdList);	
		return list;
	}
}