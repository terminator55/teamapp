package com.team.application.service;

import java.util.List;
import com.team.application.dto.DateRequestDTO;
import com.team.application.dto.TaskDTO;
import com.team.application.dto.UpdateTaskDTO;
import com.team.application.exception.BlankFieldException;
import com.team.application.exception.InvalidInputException;
import com.team.application.exception.TaskDoesNotExistException;
import com.team.application.exception.TeamDoesNotExistException;
import com.team.application.exception.UserDoesNotExistException;
import com.team.application.model.Task;

public interface TaskService {

	public void createTask(TaskDTO createTaskRequest) throws BlankFieldException, InvalidInputException, TeamDoesNotExistException, UserDoesNotExistException;

	public Task updateTask(UpdateTaskDTO updateTaskRequest) throws BlankFieldException, TaskDoesNotExistException, InvalidInputException;

	public void deleteTask(String taskId) throws TaskDoesNotExistException;
	
	public List<Task> listAllTasks();

	public List<Task> listTeamTasks(String teamId) throws TeamDoesNotExistException;  //to be used to list tasks under a team
	
	public List<Task> listUserTasks(String userId) throws UserDoesNotExistException;  //////to be used in user my profile/my tasks section

	public List<Task> listUserTeamTasks(String teamId, String userId) throws UserDoesNotExistException, TeamDoesNotExistException;  ///to be used top get user tasks under a team

	public Task viewTask(String taskId) throws TaskDoesNotExistException;
	
	public void changeDueDate(DateRequestDTO dueDateRequest) throws TaskDoesNotExistException, InvalidInputException;
	
	public void changeAssignee(String taskId, String assigneeId) throws TaskDoesNotExistException, UserDoesNotExistException;
	
	public void changeStatus(String taskId, String status) throws TaskDoesNotExistException, InvalidInputException;     //if status == completed ...task closed
	
	public List<Task> listStarredTasks(List<String> taskIdList);
}
