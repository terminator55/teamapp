package com.team.application.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.team.application.constants.AppConstants;
import com.team.application.dto.FbUser;
import com.team.application.dto.SignupDTO;
import com.team.application.dto.UserPasswordDto;
import com.team.application.exception.BlankFieldException;
import com.team.application.exception.ConfirmPasswordDoesNotMatchException;
import com.team.application.exception.FbUserException;
import com.team.application.exception.InvalidEmailException;
import com.team.application.exception.InvalidInputException;
import com.team.application.exception.InvalidMobileNumberException;
import com.team.application.exception.InvalidUsernameException;
import com.team.application.exception.UserEmailExistsException;
import com.team.application.exception.UserMobileExistsException;
import com.team.application.exception.UserNameUnavailableException;
import com.team.application.model.User;
import com.team.application.response.TeamAppResponse;
import com.team.application.service.UserService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@RestController
@Component
@Api(tags = "SignUp-API")
@RequestMapping("/signup")
public class SignUpController {

	@Autowired
	private UserService userService;

	private final Logger logger = LoggerFactory.getLogger(SignUpController.class);

	@CrossOrigin
	@ApiOperation(value = "SignUp Service", notes = "SignUp service: for user registration")
	@RequestMapping(method = RequestMethod.POST)
	@ResponseStatus(HttpStatus.CREATED)
	TeamAppResponse createUser(@RequestBody SignupDTO userSignupRequest) {
		logger.info("Inside SignUpController : createUser");
		try {
			return new TeamAppResponse(AppConstants.ResponseMessages.USER_CREATED, AppConstants.Status.SUCCESS,
					userService.userSignUp(userSignupRequest));
		} catch (BlankFieldException e) {
			return new TeamAppResponse(AppConstants.ResponseMessages.BLANK_FIELDS, AppConstants.ERROR, null);
		} catch (InvalidMobileNumberException e) {
			return new TeamAppResponse(AppConstants.ResponseMessages.INVALID_MOBILE_NUMBER, AppConstants.ERROR,null);
		} catch (UserNameUnavailableException e) {
			return new TeamAppResponse(AppConstants.ResponseMessages.USERNAME_ALREADY_TAKEN,AppConstants.ERROR, null);
		} catch (UserEmailExistsException e) {
			return new TeamAppResponse(AppConstants.ResponseMessages.USER_EMAIL_ALREADY_REGISTERED,AppConstants.ERROR, null);
		} catch (UserMobileExistsException e) {
			return new TeamAppResponse(AppConstants.ResponseMessages.USER_MOBILE_ALREADY_REGISTERED,AppConstants.ERROR, null);
		} catch (InvalidEmailException e) {
			return new TeamAppResponse(AppConstants.ResponseMessages.INVALID_EMAIL,AppConstants.ERROR, null);
		} catch (InvalidUsernameException e) {
			return new TeamAppResponse(AppConstants.ResponseMessages.INVALID_USERNAME,AppConstants.ERROR, null);
		} catch (InvalidInputException e) {
			return new TeamAppResponse
					(AppConstants.ResponseMessages.INVALID_VALUE,AppConstants.ERROR, null);
		} 
	}

	@CrossOrigin
	@ApiOperation(value = "Gmail or fb user creation Service", notes = "Gmail or fb user creation service: for gmail or fb user creation")
	@RequestMapping(value = "/social", method = RequestMethod.POST)
	@ResponseStatus(HttpStatus.CREATED)
	TeamAppResponse createFbUser(@RequestBody FbUser fbUser) {
		logger.info("Inside FbSignUpController : createUser");
		User profile = userService.createFbUser(fbUser);
		return new TeamAppResponse(AppConstants.ResponseMessages.USER_CREATED, AppConstants.Status.SUCCESS, profile);
	}

	@CrossOrigin
	@ApiOperation(value = "Forgot Password User Exist Service", notes = "Service to provide check user exist or not: forgot password")
	@RequestMapping(value = "/userexist", method = RequestMethod.GET)
	@ResponseStatus(HttpStatus.OK)
	TeamAppResponse forgotPasswordUserExist(@RequestParam("loginField") String loginField) {
		logger.info("Inside SignUpController : forgotPassword");
		try {
			return new TeamAppResponse(null, AppConstants.Status.SUCCESS, userService.userExist(loginField));
		} catch (FbUserException e) {
			return new TeamAppResponse("You used gmail or facebook for login previously", AppConstants.Status.ERROR, loginField);
		}

	}

	@CrossOrigin
	@ApiOperation(value = "Forgot password change service", notes = "Service to change password: forgot password")
	@RequestMapping(value = "/forgotpassword", method = RequestMethod.POST)
	@ResponseStatus(HttpStatus.OK)
	public TeamAppResponse forgotPassword(@RequestBody UserPasswordDto dto) {
		logger.info("Inside SignUpController : forgotPassword");
		try {
			userService.forgotPassword(dto);
			return new TeamAppResponse(AppConstants.ResponseMessages.PASSWORD_CHANGED, AppConstants.Status.SUCCESS, null);
		} catch (ConfirmPasswordDoesNotMatchException e) {
			return new TeamAppResponse(AppConstants.ResponseMessages.CONFIRM_PASSWORD_WRONG, AppConstants.Status.ERROR, "BAD_REQUEST");
			
		}
	}
}
