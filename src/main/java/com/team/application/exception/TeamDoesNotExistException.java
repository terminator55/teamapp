package com.team.application.exception;

public class TeamDoesNotExistException extends Exception {
	public TeamDoesNotExistException(String exception) {
		super(exception);
	}

	public TeamDoesNotExistException() {
		super();
	}
}
