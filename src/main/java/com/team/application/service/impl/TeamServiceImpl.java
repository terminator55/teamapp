package com.team.application.service.impl;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import com.team.application.dto.CreateTeamDTO;
import com.team.application.dto.TeamUpdateDTO;
import com.team.application.exception.BlankFieldException;
import com.team.application.exception.InvalidInputException;
import com.team.application.exception.TeamDoesNotExistException;
import com.team.application.exception.UserDoesNotExistException;
import com.team.application.model.Team;
import com.team.application.repository.TeamRepository;
import com.team.application.repository.UserRepository;
import com.team.application.service.TeamService;
import com.team.application.utils.ValidationUtils;

@Component
public class TeamServiceImpl implements TeamService {
	
	@Autowired
	TeamRepository teamRepository;
	
	@Autowired
	UserRepository userRepository;

	private final Logger log = LoggerFactory.getLogger(TeamServiceImpl.class);
	
	@Override
	public void createTeam(CreateTeamDTO createTeamRequest) throws BlankFieldException, UserDoesNotExistException, InvalidInputException {
		log.info("createTeam : TeamServiceImpl - entering");
		if ((!StringUtils.hasText(createTeamRequest.getAdminId()))
				|| (!StringUtils.hasText(createTeamRequest.getTeamName()))) {
			throw new BlankFieldException();
		} else {
		Team newTeam = new Team();
		if (!userRepository.exists(createTeamRequest.getAdminId())) {
			throw new UserDoesNotExistException();
		}
		newTeam.setAdminId(createTeamRequest.getAdminId());   ///logged in userId
		if(!ValidationUtils.isValidName(createTeamRequest.getTeamName())){
			throw new InvalidInputException();
		}
		newTeam.setTeamName(createTeamRequest.getTeamName());
		if (!StringUtils.hasText(createTeamRequest.getTeamDescription())){
			newTeam.setTeamDescription("");
		}
		newTeam.setTeamDescription(createTeamRequest.getTeamDescription());
		
			List<String> userList = createTeamRequest.getUserList();
			userList.add(createTeamRequest.getAdminId());		//add logged in user to team as default
			newTeam.setUserList(userList);

		teamRepository.save(newTeam);
		log.info("createTeam : TeamServiceImpl - leaving");
		}
		
	}

	@Override
	public Team updateTeam(String teamId, TeamUpdateDTO teamUpdateRequest) throws BlankFieldException, TeamDoesNotExistException, InvalidInputException {
		log.info("createTeam : TeamServiceImpl - entering");
		Team updateTeam = new Team();
		if ((!StringUtils.hasText(teamId))){
			throw new BlankFieldException();
		}
		else if (!teamRepository.exists(teamId)) {
			throw new TeamDoesNotExistException();
		}
		else if (!StringUtils.hasText(teamUpdateRequest.getTeamName())){
			throw new BlankFieldException();
		}
		else {
			if (!StringUtils.hasText(teamUpdateRequest.getTeamDescription())){
				teamUpdateRequest.setTeamDescription("");
			}
			updateTeam  = teamRepository.findOne(teamId);
			if(!ValidationUtils.isValidName(teamUpdateRequest.getTeamName())){
				throw new InvalidInputException();
			}
			updateTeam.setTeamName(teamUpdateRequest.getTeamName());
			updateTeam.setTeamDescription(teamUpdateRequest.getTeamDescription());
			teamRepository.save(updateTeam);
		}
		log.info("updateTeam : TeamServiceImpl - leaving");
		return updateTeam;
	}

	@Override
	public void deleteTeam(String teamId) throws TeamDoesNotExistException {
		log.info("deleteTeam : TeamServiceImpl - entering");
		if (!teamRepository.exists(teamId)) {
			throw new TeamDoesNotExistException();
		} else {
			//removeTeamMembers														//need to review
			teamRepository.delete(teamId);
		}
		log.info("deleteTeam : TeamServiceImpl - leaving");
	}

	@Override
	public List<Team> listAllTeams() {
		log.info("listAllTeams : TeamServiceImpl - entering");
		List<Team> list = teamRepository.findAll();

		log.info("listAllTeams : TeamServiceImpl - leaving");
		return list;
	}

	@Override
	public Team viewTeam(String teamId) throws TeamDoesNotExistException {
		log.info("viewTeam : TeamServiceImpl - entering");
		if (!teamRepository.exists(teamId)) {
			throw new TeamDoesNotExistException();
		} else {
		log.info("viewTeam : TeamServiceImpl - leaving");		
		return teamRepository.findOne(teamId);
		}
	}

	@Override
	public List<Team> listTeamsByUser(String userId) throws UserDoesNotExistException {
		log.info("listTeamsByUser : TeamServiceImpl - entering");
		if (!userRepository.exists(userId)) {
			throw new UserDoesNotExistException();
		}
		else{
			log.info("listTeamsByUser : TeamServiceImpl - leaving");
			return teamRepository.listTeamsByUser(userId);
		}
	}

	@Override
	public List<String> listTeamMembers(String teamId) throws TeamDoesNotExistException {
		log.info("listTeamMembers : TeamServiceImpl - entering");
		if (!teamRepository.exists(teamId)) {
			throw new TeamDoesNotExistException();
		} 
		else{
			log.info("listTeamMembers : TeamServiceImpl - leaving");
			return teamRepository.findOne(teamId).getUserList();
		}
		
	}

	@Override
	public void removeMembersFromTeam(String teamId, List<String> userIdList) throws TeamDoesNotExistException {
		log.info("removeMembersFromTeam : TeamServiceImpl - entering");
		if (!teamRepository.exists(teamId)) {
			throw new TeamDoesNotExistException();
		}
		else{
			Team newTeam = teamRepository.findOne(teamId);
			List<String> currentList = newTeam.getUserList();
			currentList.removeAll(userIdList);
			newTeam.setUserList(currentList);
			teamRepository.save(newTeam);
			log.info("removeMembersFromTeam : TeamServiceImpl - leaving");
		}
	}

	@Override
	public void addMembersToTeam(String teamId, List<String> userIdList) throws TeamDoesNotExistException {
		log.info("addMembersFromTeam : TeamServiceImpl - entering");
		if (!teamRepository.exists(teamId)) {
			throw new TeamDoesNotExistException();
		}
		Team newTeam = teamRepository.findOne(teamId);
		List<String> currentList = newTeam.getUserList();
		currentList.addAll(userIdList);
		newTeam.setUserList(currentList);
		teamRepository.save(newTeam);
		log.info("addMembersFromTeam : TeamServiceImpl - leaving");
	}				

	@Override
	public List<Team> listStarredTeams(List<String> teamIdList) {
		List<Team> list = teamRepository.listStarredTeams(teamIdList);

		return list;
	}
}
