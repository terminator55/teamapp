db.createCollection("task",{taskId: ObjectId, taskName: String, status: String, taskDescription: String, taskCreatedOn: Date, taskUpdatedOn : Date, taskDueDate: Date, creatorId: String, creatorName: String, assigneeId: String, teamId: String})

db.createCollection("team",{teamId: ObjectId, teamName: String, teamDescription: String, adminId: String, userList: Array})

db.createCollection("user",{userId: String, userName: String, firstName: String, lastName: String, emailId: String, imageUrl: String, mobileNo: String,password:String,createdOn:Date,updatedOn:Date,teamList: Array,taskList: Array})
