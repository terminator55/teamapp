package com.team.application.exception;

public class UserEmailExistsException extends Exception {
	public UserEmailExistsException(String exception) {
		super(exception);
	}

	public UserEmailExistsException () {
		super();
		
	}
}
