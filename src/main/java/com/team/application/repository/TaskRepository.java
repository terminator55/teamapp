package com.team.application.repository;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.stereotype.Repository;

import com.team.application.model.Task;

@Repository
public interface TaskRepository extends MongoRepository<Task, String> {

	@Query("{ 'teamId' : ?0 }")
	List<Task> listTeamTasks(String teamId);

	@Query("{ 'teamId' : ?0, 'assigneeId' : ?1 }")
	List<Task> listUserTeamTasks(String teamId, String userId);

	@Query("{ 'assigneeId' : ?0 }")
	List<Task> listUserTasks(String userId);

	@Query("{ _id: { $in: ?0}}")
	List<Task> listStarredTasks(List<String> taskIdList);

}
