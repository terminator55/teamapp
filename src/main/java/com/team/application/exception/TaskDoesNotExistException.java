package com.team.application.exception;

public class TaskDoesNotExistException extends Exception {
	public TaskDoesNotExistException(String exception) {
		super(exception);
	}

	public TaskDoesNotExistException() {
		super();
	}
}
