package com.team.application.repository;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.stereotype.Repository;
import com.team.application.model.User;

@Repository
public interface UserRepository extends MongoRepository<User, String> {

	@Query("{ mobileNo : ?0}")
	User findByMobileNo(String mobileNo);

	@Query("{ emailId : ?0}")
	User findByEmailId(String emailId);

	@Query("{ userName : ?0}")
	User findByUserName(String userName);

	@Query("{'_id': ?0 , {$set:{'password': ?1 }}}")
	void changeUserPassword(String userId, String hashedPassword);

}