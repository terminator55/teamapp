package com.team.application.service;

import java.util.List;

import com.team.application.dto.FbUser;
import com.team.application.dto.LoginDTO;
import com.team.application.dto.SignupDTO;
import com.team.application.dto.UpdateProfileDTO;
import com.team.application.dto.UserPasswordDto;
import com.team.application.exception.BlankFieldException;
import com.team.application.exception.ConfirmPasswordDoesNotMatchException;
import com.team.application.exception.FbUserException;
import com.team.application.exception.InvalidEmailException;
import com.team.application.exception.InvalidInputException;
import com.team.application.exception.InvalidMobileNumberException;
import com.team.application.exception.InvalidUsernameException;
import com.team.application.exception.OldPasswordException;
import com.team.application.exception.UserDoesNotExistException;
import com.team.application.exception.UserEmailExistsException;
import com.team.application.exception.UserMobileExistsException;
import com.team.application.exception.UserNameUnavailableException;
import com.team.application.exception.WrongPasswordException;
import com.team.application.model.User;
import com.team.application.response.UserExistResponse;

public interface UserService {

	public String userSignUp(SignupDTO signUpRequest) throws BlankFieldException, InvalidMobileNumberException, UserNameUnavailableException, UserEmailExistsException, UserMobileExistsException, InvalidEmailException, InvalidUsernameException, InvalidInputException;
	
	public User createFbUser(FbUser fbUser);

	public User userLogin(LoginDTO loginRequest) throws UserDoesNotExistException, BlankFieldException, WrongPasswordException, FbUserException, InvalidMobileNumberException;

	public void deleteUser(String userId) throws UserDoesNotExistException;
	
	public UserExistResponse userExist(String loginField) throws FbUserException;
	
	public User viewUserProfile(String userId) throws UserDoesNotExistException;
	
	public User viewOtherUserProfile(String userId, String otherUserId) throws UserDoesNotExistException;

	public List<User> listAllUsers();	

	public void forgotPassword(UserPasswordDto passwordRequest) throws ConfirmPasswordDoesNotMatchException;
	
	public void changePassword(UserPasswordDto passwordRequest) throws UserDoesNotExistException, FbUserException, WrongPasswordException, OldPasswordException, ConfirmPasswordDoesNotMatchException;

/*	public String uploadProfileImage(MultipartFile file, String userId)
			throws ImageExceedsSizeException, IOException, UserDoesNotExistException;*/
	
	public User editUserProfile(String userId, UpdateProfileDTO updateProfileRequest) throws UserDoesNotExistException, BlankFieldException, InvalidInputException;
	
	public User editEmail(String userId,String email) throws BlankFieldException, UserDoesNotExistException, InvalidEmailException; 
	
	public User editMobile(String userId,String mobile) throws BlankFieldException, UserDoesNotExistException, InvalidMobileNumberException; 
	
	public User editUsername(String userId,String username) throws BlankFieldException, UserDoesNotExistException, InvalidUsernameException; 
}
