package com.team.application.service.impl;

import java.util.Date;
import java.util.List;
import java.util.UUID;

import org.apache.commons.lang3.math.NumberUtils;
import org.mindrot.jbcrypt.BCrypt;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import com.team.application.constants.AppConstants;
import com.team.application.dto.FbUser;
import com.team.application.dto.LoginDTO;
import com.team.application.dto.SignupDTO;
import com.team.application.dto.UpdateProfileDTO;
import com.team.application.dto.UserPasswordDto;
import com.team.application.exception.BlankFieldException;
import com.team.application.exception.ConfirmPasswordDoesNotMatchException;
import com.team.application.exception.FbUserException;
import com.team.application.exception.InvalidEmailException;
import com.team.application.exception.InvalidInputException;
import com.team.application.exception.InvalidMobileNumberException;
import com.team.application.exception.InvalidUsernameException;
import com.team.application.exception.OldPasswordException;
import com.team.application.exception.UserDoesNotExistException;
import com.team.application.exception.UserEmailExistsException;
import com.team.application.exception.UserMobileExistsException;
import com.team.application.exception.UserNameUnavailableException;
import com.team.application.exception.WrongPasswordException;
import com.team.application.model.User;
import com.team.application.repository.UserRepository;
import com.team.application.response.UserExistResponse;
import com.team.application.service.UserService;
import com.team.application.utils.ValidationUtils;

//@Service(value = "userService")
@Component
public class UserServiceImpl implements UserService {
	
	@Autowired
	UserRepository userRepository;
	
	private final Logger log = LoggerFactory.getLogger(UserServiceImpl.class);
	

	@Override
	public String userSignUp(SignupDTO signUpRequest) throws BlankFieldException, InvalidMobileNumberException, UserNameUnavailableException, UserEmailExistsException, UserMobileExistsException, InvalidEmailException, InvalidUsernameException, InvalidInputException {
		log.info("UserSignUp service : entering");

		if (!((StringUtils.hasText(signUpRequest.getEmailId()) || StringUtils.hasText(signUpRequest.getMobileNo()) || StringUtils.hasText(signUpRequest.getUserName()))
				&& StringUtils.hasText(signUpRequest.getFirstName()) && StringUtils.hasText(signUpRequest.getLastName())
				&& StringUtils.hasText(signUpRequest.getPassword()))) {
			log.info("Blank fields in signup!");
			log.info("UserSignUp service : exiting");
			throw new BlankFieldException("Fields cannot be blank");
		}

		if (StringUtils.hasText(signUpRequest.getUserName())) {
			if(ValidationUtils.isValidUsername(signUpRequest.getUserName())) {
				if (!(userRepository.findByUserName(signUpRequest.getUserName()) == null)) {
					log.info("UserName is already taken");
					log.info("UserSignUp service : exiting");
					throw new UserNameUnavailableException("UserName already taken");
				}
			}
			else {
				log.info("Username is invalid");
				log.info("UserSignUp service : exiting");
				throw new InvalidUsernameException("Username is invalid!");
			}
		}

		if (StringUtils.hasText(signUpRequest.getEmailId())) {
			if (ValidationUtils.isValidEmail(signUpRequest.getEmailId())) {
				if (!(userRepository.findByEmailId(signUpRequest.getEmailId()) == null)) {
					log.info("User already exists with emailId");
					log.info("UserSignUp service : exiting");
					throw new UserEmailExistsException("UsereEmail already registered");
				}
			}
			else {
				log.info("Email Id is invalid");
				log.info("UserSignUp service : exiting");
				throw new InvalidEmailException("Email Id is invalid!");
			}
		}
		
		if (StringUtils.hasText(signUpRequest.getMobileNo())) {
			if (ValidationUtils.isValidMobileNumber(signUpRequest.getMobileNo())) {
				if (!(userRepository.findByMobileNo(signUpRequest.getMobileNo()) == null)) {
					log.info("User already exists with MobileNo");
					log.info("UserSignUp service : exiting");
					throw new UserMobileExistsException("User mobile already registered");
				}
			}
			else {
				log.info("Mobile no. is invalid");
				log.info("UserSignUp service : exiting");
				throw new InvalidMobileNumberException("Mobile no. is invalid!");
			}
		}

		User user = new User();
		String uid = UUID.randomUUID().toString();
		user.setUserId(uid);
		if(!(ValidationUtils.isValidName(signUpRequest.getFirstName()) 
				&& ValidationUtils.isValidName(signUpRequest.getLastName()))) {
					throw new InvalidInputException();
		}
		user.setFirstName(signUpRequest.getFirstName().trim());
		user.setLastName(signUpRequest.getLastName().trim());
		
		if (!StringUtils.hasText(signUpRequest.getUserName())) {
			user.setUserName("");
			log.info("UserSignUp service : empty UserName field");
		}
		if (!StringUtils.hasText(signUpRequest.getEmailId())) {
			user.setEmailId("");
			log.info("UserSignUp service : empty EmailId field");
		}
		if (!StringUtils.hasText(signUpRequest.getMobileNo())) {
			user.setMobileNo("");
			log.info("UserSignUp service : empty mobileNo field");
		}
		user.setUserName(signUpRequest.getUserName().trim());
		user.setEmailId(signUpRequest.getEmailId().trim());
		user.setMobileNo(signUpRequest.getMobileNo().trim());
		
		String hashedPassword = BCrypt.hashpw(signUpRequest.getPassword().trim(), BCrypt.gensalt());
		user.setPassword(hashedPassword);
		
		user.setCreatedOn(new Date());
		user.setUpdatedOn(new Date());
		
/*		user.setTaskList(Collections.<String>emptyList());    //set empty list at the time of user creation
		user.setTeamList(Collections.<String>emptyList());	  //set empty list at the time of user creation
*/
		userRepository.save(user);

 		log.info("UserSignUp service : leaving");
		return uid;
	}

	@Override
	public User createFbUser(FbUser fbUser) {
		log.info("CreatUser service : entering");
		User user = new User();
		/*User profile = new User();*/
		if ((userRepository.findByEmailId(fbUser.getEmailId()) == null) 
				&& (userRepository.findByMobileNo(fbUser.getMobileNo()) == null)
				 && (userRepository.findByUserName(fbUser.getUserName()) == null)) {

			log.info("CreateUser service : creating fbUser");
			
			String uid = UUID.randomUUID().toString();
			user.setUserId(uid);
			// fb username 
			if (!StringUtils.hasText(fbUser.getUserName())) {
				user.setUserName("");
				log.info("CreateFbUser service : empty UserName field");
			}
			if (!StringUtils.hasText(fbUser.getEmailId())) {
				user.setEmailId("");
				log.info("CreateFbUser service : empty EmailId field");
			}
			if (!StringUtils.hasText(fbUser.getMobileNo())) {
				user.setMobileNo("");
				log.info("CreateFbUser service : empty mobileNo field");
			}
			user.setUserName(fbUser.getUserName()); 
			user.setEmailId(fbUser.getEmailId());
			user.setMobileNo(fbUser.getMobileNo());
			user.setFirstName(fbUser.getFirstName());
			user.setLastName(fbUser.getLastName());
			
			String hashedPassword = BCrypt.hashpw(uid, BCrypt.gensalt());
			user.setPassword(hashedPassword);
			user.setImageUrl(fbUser.getImageUrl());
			
			user.setCreatedOn(new Date());
			user.setUpdatedOn(new Date());
			
/*			user.setTaskList(Collections.<String>emptyList());    //set empty list at the time of user creation
			user.setTeamList(Collections.<String>emptyList());	  //set empty list at the time of user creation
*/
			userRepository.save(user);

			return user;
		}

		else {
			if(!(userRepository.findByUserName(fbUser.getUserName()) == null)){
				log.info("CreateUser service : fbUser with UserName already exists");
				user = userRepository.findByUserName(fbUser.getUserName());
			}
			
			if(!(userRepository.findByEmailId(fbUser.getEmailId()) == null)){
				log.info("CreateUser service : fbUser with email already exists");
				user = userRepository.findByEmailId(fbUser.getEmailId());
			}
			
			if(!(userRepository.findByMobileNo(fbUser.getMobileNo()) == null)){
				log.info("CreateUser service : fbUser with mobile no already exists");
				user = userRepository.findByMobileNo(fbUser.getMobileNo());
			}
		}
		return user;
	}

	@Override
	public User userLogin(LoginDTO userCredentials) throws UserDoesNotExistException, BlankFieldException, WrongPasswordException, FbUserException, InvalidMobileNumberException {
		log.info("UserLogin service : entering");

		if (!(StringUtils.hasText(userCredentials.getLoginField())
				&& StringUtils.hasText(userCredentials.getPassword()))) {
			log.info("Blank fields in login!");
			log.info("UserLogin service : exiting");
			throw new BlankFieldException("Fields cannot be blank");
		}
		
		if (NumberUtils.isNumber((userCredentials.getLoginField().trim()))) {
			if(ValidationUtils.isValidMobileNumber(userCredentials.getLoginField().trim())) {
				log.info("Login Field : Mobile No");
				if (userRepository.findByMobileNo(userCredentials.getLoginField().trim()) == null) {
					log.info("user does not exist");
					log.info("UserLogin service : exiting");
					throw new UserDoesNotExistException("user does not exist : mobile number not registered");
				}
			}
			else {
				log.info("Mobile no. is invalid");
				log.info("UserSignUp service : exiting");
				throw new InvalidMobileNumberException("Mobile no. is invalid!");
			}
			
			User userTest = userRepository.findByMobileNo(userCredentials.getLoginField().trim());
			if (!BCrypt.checkpw(userCredentials.getPassword().trim(), userTest.getPassword())) {
				log.info("wrong password!");
				log.info("UserLogin service : exiting");
				throw new WrongPasswordException();
			} else {
				log.info("UserLogin service : exiting");
				User profile = viewUserProfile(userTest.getUserId());
				return profile;
			}

		} else {
			log.info("Login Field : Emailid");
			if ((userRepository.findByEmailId(userCredentials.getLoginField().trim()) == null) && (userRepository.findByUserName(userCredentials.getLoginField().trim()) == null)) {
				log.info("user does not exist");
				log.info("UserLogin service : exiting");
				throw new UserDoesNotExistException("user does not exist");
			}

			User userTest = new User();
			if(!(userRepository.findByEmailId(userCredentials.getLoginField().trim()) == null)){
				userTest = userRepository.findByEmailId(userCredentials.getLoginField().trim());
			}
			
			if(!(userRepository.findByUserName(userCredentials.getLoginField().trim()) == null)){
				userTest = userRepository.findByUserName(userCredentials.getLoginField().trim());
			}
			
			
			
			/////////////test password/////////////////////////////
			if (BCrypt.checkpw(userTest.getUserId(), userTest.getPassword())) {
				log.info("UserLogin service : exiting");
				throw new FbUserException();
			}
			if (!BCrypt.checkpw(userCredentials.getPassword(), userTest.getPassword())) {
				log.info("wrong password!");
				log.info("UserLogin service : exiting");
				throw new WrongPasswordException();
			} else {
				log.info("UserLogin service : exiting");
				User profile = viewUserProfile(userTest.getUserId());
				return profile;
			}
		}
	}

	@Override
	public void deleteUser(String userId) throws UserDoesNotExistException {
		log.info("deleteUser : UserServiceImpl - entering");
		if (!userRepository.exists(userId)) {
			throw new UserDoesNotExistException();
		} else {
			//removeTeamMembers														//need to review
			userRepository.delete(userId);
		}
		log.info("deleteUser : UserServiceImpl - leaving");
	}
	
	@Override
	public UserExistResponse userExist(String loginField) throws FbUserException {
		log.info("userExist : UserServiceImpl - entering");
		if (NumberUtils.isNumber(loginField)) {
			if (userRepository.findByMobileNo(loginField) == null)
				return new UserExistResponse(AppConstants.FALSE,
						AppConstants.ResponseMessages.USER_DOES_NOT_EXIST_WITH_MOBILE, AppConstants.FALSE, null);
			else {
				User user = userRepository.findByMobileNo(loginField);
				return new UserExistResponse(AppConstants.TRUE,
						AppConstants.ResponseMessages.USER_EXISTS_WITH_THESE_CREDENTIALS, AppConstants.TRUE, user.getUserId());
			}
		}

		else {
			if ((userRepository.findByEmailId(loginField.trim()) == null) && (userRepository.findByUserName(loginField.trim()) == null))
				return new UserExistResponse(AppConstants.FALSE,
						AppConstants.ResponseMessages.USER_DOES_NOT_EXIST_WITH_EMAIL, AppConstants.FALSE, null);
			else {
				//String userId = userByEmailRepository.findUserIdByEmailId(loginField);

				User userTest = new User();
				if(!(userRepository.findByEmailId(loginField.trim()) == null)){
					userTest = userRepository.findByEmailId(loginField.trim());
				}
				
				if(!(userRepository.findByUserName(loginField.trim()) == null)){
					userTest = userRepository.findByUserName(loginField.trim());
				}
				
				
				if (BCrypt.checkpw(userTest.getUserId(), userTest.getPassword())) {
					log.info("UserExist service : exiting");
					throw new FbUserException();

				}
				log.info("userExist : UserServiceImpl - leaving");
				return new UserExistResponse(AppConstants.TRUE,
						AppConstants.ResponseMessages.USER_EXISTS_WITH_THESE_CREDENTIALS, AppConstants.FALSE, userTest.getUserId());
			}
		}
	}

	@Override
	public User viewUserProfile(String userId) throws UserDoesNotExistException {
		if (!userRepository.exists(userId)) {
			log.info("viewUserProfile : UserServiceImpl - leaving");
			throw new UserDoesNotExistException();
		}
		else{
			log.info("viewUserProfile : UserServiceImpl - leaving");
			return userRepository.findOne(userId);
		}
	}

	@Override
	public List<User> listAllUsers() {
		log.info("listAllUsers : UserServiceImpl - entering");
		List<User> list = (List<User>) userRepository.findAll();
		log.info("listAllUsers : UserServiceImpl - leaving");
		return list;
	}

	@Override
	public User viewOtherUserProfile(String userId, String otherUserId) throws UserDoesNotExistException {
		log.info("viewOtherUserProfile : UserServiceImpl - entering");
		if (userRepository.findOne(userId.trim()) == null
				&& userRepository.findOne(otherUserId.trim()) == null) {
			log.info("user does not exist");
			log.info("viewOtherUserProfile : UserServiceImpl - exiting");
			throw new UserDoesNotExistException("User does not exist!");
		}
		User otherUserProfile = viewUserProfile(otherUserId.trim());
		log.info("viewOtherUserProfile : UserServiceImpl - exiting");
		return otherUserProfile;
	}
//
	@Override
	public void forgotPassword(UserPasswordDto passwordRequest) throws ConfirmPasswordDoesNotMatchException {
		if (!passwordRequest.getNewPassword().contentEquals(passwordRequest.getConfirmPassword())) {
			log.info("confirm password wrong");
			log.info("ChangePassword service : exiting");
			throw new ConfirmPasswordDoesNotMatchException();
		}

		User userTest = userRepository.findOne(passwordRequest.getUserId().trim());
		String hashedPassword = BCrypt.hashpw(passwordRequest.getNewPassword().trim(), BCrypt.gensalt());
		/*userRepository.changeUserPassword(passwordRequest.getUserId().trim(), hashedPassword);*/
		userTest.setPassword(hashedPassword);
		userRepository.save(userTest);
		log.info("ChangePassword service : password updated");

		log.info("ChangePassword service : leaving");
		
	}

	@Override
	public void changePassword(UserPasswordDto passwordRequest) throws UserDoesNotExistException, FbUserException, WrongPasswordException, OldPasswordException, ConfirmPasswordDoesNotMatchException {
		log.info("ChangePassword service : entering");

		if (userRepository.findOne(passwordRequest.getUserId().trim()) == null) {
			log.info("user does not exist");
			log.info("ChangePassword service : exiting");
			throw new UserDoesNotExistException("User does not exist!");
		}

		User userTest = userRepository.findOne(passwordRequest.getUserId().trim());
		if (BCrypt.checkpw(userTest.getUserId(), userTest.getPassword())) {
			log.info("UserLogin service : exiting");
			throw new FbUserException();
		}
		if (!BCrypt.checkpw(passwordRequest.getCurrentPassword().trim(), userTest.getPassword())) {
			log.info("wrong password");
			log.info("ChangePassword service : exiting");
			throw new WrongPasswordException();
		}

		if (BCrypt.checkpw(passwordRequest.getNewPassword().trim(), userTest.getPassword())) {
			log.info("old password");
			log.info("ChangePassword service : exiting");
			throw new OldPasswordException();
		}

		if (!passwordRequest.getNewPassword().contentEquals(passwordRequest.getConfirmPassword())) {
			log.info("confirm password wrong");
			log.info("ChangePassword service : exiting");
			throw new ConfirmPasswordDoesNotMatchException();
		}

		String hashedPassword = BCrypt.hashpw(passwordRequest.getNewPassword().trim(), BCrypt.gensalt());
		/*userRepository.changeUserPassword(passwordRequest.getUserId().trim(), hashedPassword);*/
		userTest.setPassword(hashedPassword);
		userRepository.save(userTest);
		log.info("ChangePassword service : password updated");
		log.info("ChangePassword service : leaving");
	}

/*	@Override
	public String uploadProfileImage(MultipartFile file, String userId)
	if (userRepository.findOne(userId) == null) {
		log.info("user does not exist");
		log.info("uploadProfileImage service : exiting");
		throw new UserDoesNotExistException("User does not exist!");
	}
	if (file.isEmpty()) {
		throw new IOException();
	}

	String fileName = amazonClient.uploadFile(file);
	
	 * if (file.getSize() > 15000000) { throw new ImageExceedsSizeException(
	 * "Image size exceeded!"); }
	 
	
	 * byte[] bytes = file.getBytes(); Path path = Paths.get(userBucketPath +
	 * file.getOriginalFilename()); Files.write(path, bytes);
	 
	log.info("Writing image file");
	userDetailsRepository.changeUserProfileImage(fileName, userId);
	List<ReviewByUser> list = reviewUserRepository.findByUserId(userId);
	for (ReviewByUser r : list) {
		reviewProductRepository.changeReviewerImage(fileName, r.getProductId(), r.getReviewId());
	}

	List<RecentReview> recentList = (List<RecentReview>) recentReviewRepository.findAll();
	for (RecentReview recent : recentList) {
		if (recent.getUserId().contentEquals(userId)) {
			recentReviewRepository.changeReviewerImage(fileName, recent.getReviewId());
		}
	}
	return fileName;
	}*/

	@Override
	public User editUserProfile(String userId, UpdateProfileDTO updateProfileRequest) throws UserDoesNotExistException, BlankFieldException, InvalidInputException {
			log.info("editUserProfile service : entering");
			User user;
			if ((!StringUtils.hasText(userId))){
				throw new BlankFieldException();
			}
			if (!userRepository.exists(userId)) {
				throw new UserDoesNotExistException();
			}
			if(!(StringUtils.hasText(updateProfileRequest.getFirstName()) || StringUtils.hasText(updateProfileRequest.getFirstName()))) {
				throw new BlankFieldException();
			}
			else{
				if(!(ValidationUtils.isValidName(updateProfileRequest.getFirstName()) 
					&& ValidationUtils.isValidName(updateProfileRequest.getLastName()))) {
						throw new InvalidInputException();
				}
				else {
					user = userRepository.findOne(userId);
					user.setFirstName(updateProfileRequest.getFirstName());
					user.setLastName(updateProfileRequest.getLastName());
					userRepository.save(user);
				}
			}
			log.info("editUserProfile service : leaving");
			return user;
	}

	@Override
	public User editEmail(String userId, String email) throws BlankFieldException, UserDoesNotExistException, InvalidEmailException {
		log.info("editEmail service : entering");
		if ((!StringUtils.hasText(userId))){
			throw new BlankFieldException();
		}
		if (!userRepository.exists(userId)) {
			throw new UserDoesNotExistException();
		}
		if (!ValidationUtils.isValidEmail(email)) {
			throw new InvalidEmailException("Email Id is invalid!");
		}
		User editUser =  userRepository.findOne(userId);
		editUser.setEmailId(email);
		userRepository.save(editUser);
		log.info("editEmail service : leaving");
		return editUser;
	}

	@Override
	public User editMobile(String userId, String mobile) throws BlankFieldException, UserDoesNotExistException, InvalidMobileNumberException {
		log.info("editMobile service : entering");
		if ((!StringUtils.hasText(userId))){
			throw new BlankFieldException();
		}
		if (!userRepository.exists(userId)) {
			throw new UserDoesNotExistException();
		}
		if (!ValidationUtils.isValidMobileNumber(mobile)) {
			throw new InvalidMobileNumberException();
		}
		User editUser =  userRepository.findOne(userId);
		editUser.setMobileNo(mobile);
		userRepository.save(editUser);
		log.info("editMobile service : leaving");
		return editUser;
	}

	@Override
	public User editUsername(String userId, String username) throws BlankFieldException, UserDoesNotExistException, InvalidUsernameException {
		log.info("editUsername service : entering");
		if ((!StringUtils.hasText(userId))){
			throw new BlankFieldException();
		}
		if (!userRepository.exists(userId)) {
			throw new UserDoesNotExistException();
		}
		if (!ValidationUtils.isValidUsername(username)) {
			throw new InvalidUsernameException("Username is invalid!");
		}
		User editUser =  userRepository.findOne(userId);
		editUser.setUserName(username);
		userRepository.save(editUser);
		log.info("editUsername service : leaving");
		return editUser;
	}
}
