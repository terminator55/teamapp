package com.team.application.controller;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.team.application.constants.AppConstants;
import com.team.application.dto.CreateTeamDTO;
import com.team.application.dto.IdListRequestDTO;
import com.team.application.dto.TeamUpdateDTO;
import com.team.application.exception.BlankFieldException;
import com.team.application.exception.InvalidInputException;
import com.team.application.exception.TeamDoesNotExistException;
import com.team.application.exception.UserDoesNotExistException;
import com.team.application.model.Team;
import com.team.application.response.TeamAppResponse;
import com.team.application.service.TeamService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@RestController
@Component
@Api(tags = "Team-API")
@RequestMapping("/teams")
public class TeamController {

	@Autowired
	TeamService teamService;

	@ApiOperation(value = "Create Team", notes = "Create a new team")
	@RequestMapping(value = "/create",method = RequestMethod.POST)
	@ResponseStatus(HttpStatus.CREATED)
	public TeamAppResponse createTeam(@RequestBody CreateTeamDTO createTeamDTO) throws BlankFieldException, UserDoesNotExistException, InvalidInputException {
		try {
			teamService.createTeam(createTeamDTO);
			return new TeamAppResponse("New team Created",AppConstants.Status.SUCCESS, null);
		} catch (BlankFieldException e) {
			return new TeamAppResponse("Resource field(s) null", AppConstants.ERROR, null);
		} catch (UserDoesNotExistException e) {
			return new TeamAppResponse("Admin id does't exist in user table", AppConstants.ERROR, null);
		} catch (InvalidInputException e) {
			return new TeamAppResponse("Invalid input value(s)", AppConstants.ERROR, null);
		}
	}
	
	@ApiOperation(value = "Update Team", notes = "Update Team details")
	@RequestMapping(value = "update/{teamId}", method = RequestMethod.PUT)
	@ResponseStatus(HttpStatus.OK)
	public TeamAppResponse updateTeam(@PathVariable String teamId, TeamUpdateDTO teamUpdateDTO) throws BlankFieldException, TeamDoesNotExistException, InvalidInputException{
		try {
			return new TeamAppResponse("New team Created",AppConstants.Status.SUCCESS, teamService.updateTeam(teamId, teamUpdateDTO));
		} catch (BlankFieldException e) {
			return new TeamAppResponse("Resource field(s) null", AppConstants.ERROR, null);
		} catch (TeamDoesNotExistException e) {
			return new TeamAppResponse("Team Id doesn't exist", AppConstants.ERROR, null);
		} catch (InvalidInputException e) {
			return new TeamAppResponse("Invalid input value(s)", AppConstants.ERROR, null);
		}
	}
	
	@ApiOperation(value = "Delete Team", notes = "Delete Team")
	@RequestMapping(value = "delete/{teamId}", method = RequestMethod.DELETE)
	@ResponseStatus(HttpStatus.OK)
	public TeamAppResponse deleteTeam(@PathVariable String teamId) throws TeamDoesNotExistException {
		try {
			teamService.deleteTeam(teamId);		
			return new TeamAppResponse("Team deleted",AppConstants.Status.SUCCESS, null);
		} catch (TeamDoesNotExistException e) {
			return new TeamAppResponse("Team Id doesn't exist", AppConstants.ERROR, null);
		}
	}
	
	@ApiOperation(value = "List all Teams", notes = "List all teams")
	@RequestMapping(method = RequestMethod.GET)
	@ResponseStatus(HttpStatus.OK)
	public TeamAppResponse listAllTeams() {
		return new TeamAppResponse("List all Teams",AppConstants.Status.SUCCESS, teamService.listAllTeams());
	}
	
	@ApiOperation(value = "View Team", notes = "View team by Id")
	@RequestMapping(value = "/{teamId}", method = RequestMethod.POST)
	@ResponseStatus(HttpStatus.OK)
	public TeamAppResponse viewTeam(@PathVariable String teamId) throws TeamDoesNotExistException {
		try {
			return new TeamAppResponse("View Team",AppConstants.Status.SUCCESS, teamService.viewTeam(teamId));
		} catch (TeamDoesNotExistException e) {
			return new TeamAppResponse("Team Id doesn't exist", AppConstants.ERROR, null);
		}
	}
	
	@ApiOperation(value = "List user teams", notes = "List teams by userId")
	@RequestMapping(value = "/listUserTeams/{userId}",method = RequestMethod.POST)
	@ResponseStatus(HttpStatus.OK)
	public TeamAppResponse listTeamsByUser(@PathVariable String userId) throws UserDoesNotExistException {
		try {
			return new TeamAppResponse("List user teams",AppConstants.Status.SUCCESS, teamService.listTeamsByUser(userId));
		} catch (UserDoesNotExistException e) {
			return new TeamAppResponse("User Id does't exist", AppConstants.ERROR, null);
		}
	}
	
	@ApiOperation(value = "List team members", notes = "List members by teamId")
	@RequestMapping(value = "/listTeamMembers/{teamId}",method = RequestMethod.POST)
	@ResponseStatus(HttpStatus.OK)
	public TeamAppResponse listTeamMembers(@PathVariable String teamId) throws TeamDoesNotExistException {
		try {
				return new TeamAppResponse("List user teams",AppConstants.Status.SUCCESS, teamService.listTeamMembers(teamId));
			} catch (TeamDoesNotExistException e) {
				return new TeamAppResponse("Team Id does't exist", AppConstants.ERROR, null);
			}
	}
	
	@ApiOperation(value = "Remove team members", notes = "Remove members from team")
	@RequestMapping(value = "removeMembers/{teamId}", method = RequestMethod.PUT)
	@ResponseStatus(HttpStatus.OK)
	public TeamAppResponse removeMembersFromTeam(@PathVariable String teamId, IdListRequestDTO removeMemberListRequest) throws TeamDoesNotExistException {
			try {
				teamService.removeMembersFromTeam(teamId, removeMemberListRequest.getIdList());
				return new TeamAppResponse("Remove team members",AppConstants.Status.SUCCESS, removeMemberListRequest.getIdList());
			} catch (TeamDoesNotExistException e) {
				return new TeamAppResponse("Team Id does't exist", AppConstants.ERROR, null);
			}
	}

	@ApiOperation(value = "Add team members", notes = "Add members to team")
	@RequestMapping(value = "AddMembers/{teamId}", method = RequestMethod.PUT)
	@ResponseStatus(HttpStatus.OK)
	public TeamAppResponse addMembersToTeam(@PathVariable String teamId, IdListRequestDTO addMemberListRequest) throws TeamDoesNotExistException {
			try {
				teamService.addMembersToTeam(teamId, addMemberListRequest.getIdList());
				return new TeamAppResponse("Add team members",AppConstants.Status.SUCCESS, addMemberListRequest.getIdList());
			} catch (TeamDoesNotExistException e) {
				return new TeamAppResponse("Team Id does't exist", AppConstants.ERROR, null);
			}
	}
	
	@ApiOperation(value = "List starred teams", notes = "List starred teams")
	@RequestMapping(value = "/starred", method = RequestMethod.POST)
	@ResponseStatus(HttpStatus.OK)
	public TeamAppResponse listStarredTasks(@RequestBody IdListRequestDTO starredTeamRequest){
		return new TeamAppResponse("List starred teams",AppConstants.Status.SUCCESS, teamService.listStarredTeams(starredTeamRequest.getIdList()));
	}
}
