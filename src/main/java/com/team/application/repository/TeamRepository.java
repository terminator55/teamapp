package com.team.application.repository;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.stereotype.Repository;

import com.team.application.model.Team;

@Repository
public interface TeamRepository extends MongoRepository<Team, String> {

	@Query("{ userList : ?0}")
	List<Team> listTeamsByUser(String userId);

	@Query("{ _id: { $in: ?0}}")
	List<Team> listStarredTeams(List<String> teamIdList);
}
