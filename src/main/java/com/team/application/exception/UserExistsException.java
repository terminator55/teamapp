package com.team.application.exception;

public class UserExistsException extends Exception {
	public UserExistsException(String exception) {
		super(exception);
	}

	public UserExistsException() {
		super();
	}
}
